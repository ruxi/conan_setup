# ConanSetup

To help with setting up conan conf between box_jellyfish_framework and its users

Install
-------

Run
    ./setup.sh
    
    
This script installs the chosen conan configuration (remotes and profiles in this case) in a predetermined conan folder.
This folder will be common to all the projects that use box_jellyfish_framework, and it is used by the framework itself.
It's sole purpose, really, is to have a common way of setting the remotes, profiles, conan user for the projects that wish to use box_jellyfish_framework.

TODO: find a nicer way to declare a conan default folder.