export CONAN_USER_HOME="/c/code_utils/default_conan_folder"

# zips the contents of the config folder to be able to install it in conan from one go
python zipconfig.py
conan config install ./config.zip

# cleanup
rm config.zip

# setting up the conan user (this has privileges to upload/download from our artifactory)
conan user -p MauveStinger -r box-jellyfish-artifactory box_jellyfish_user